
import 'dart:convert';

class CheckInModel {
  int code;
  bool data;
  String error;

  CheckInModel({
    required this.code,
    required this.data,
    required this.error,
  });

  Map<String, dynamic> toMap() {
    return {
      'code': code,
      'data': data,
      'error': error,
    };
  }

  factory CheckInModel.fromMap(Map<String, dynamic> map) {
    return CheckInModel(
      code: map['code'] ?? '',
      data: map['data'] ?? '',
      error: map['error'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory CheckInModel.fromJson(String source) => CheckInModel.fromMap(json.decode(source));

}
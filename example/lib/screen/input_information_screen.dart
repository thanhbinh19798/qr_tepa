import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import '../home_controller.dart';

class InputInformation extends GetWidget<HomeController> {
  InputInformation({Key? key}) : super(key: key);
  final HomeController homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: TextField(
                controller: homeController.username,
                onChanged: (value) {},
                decoration: InputDecoration(
                  labelText: 'Họ và tên',
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  hintStyle: const TextStyle(color: Colors.grey),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: const BorderSide(color: Colors.blue),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: const BorderSide(color: Colors.greenAccent)),
                  isDense: true,
                  contentPadding: const EdgeInsets.fromLTRB(5, 20, 5, 20),
                ),
                cursorColor: Colors.black,
                style: const TextStyle(fontSize: 20, color: Colors.black),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: TextField(
                controller: homeController.phoneMobile,
                keyboardType: TextInputType.number,
                onChanged: (value) {},
                decoration: InputDecoration(
                  labelText: 'Số điện thoại',
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  hintStyle: const TextStyle(color: Colors.grey),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: const BorderSide(color: Colors.blue),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      borderSide: const BorderSide(color: Colors.greenAccent)),
                  isDense: true,
                  // Added this
                  contentPadding: const EdgeInsets.fromLTRB(5, 20, 5, 20),
                ),
                cursorColor: Colors.black,
                style: const TextStyle(fontSize: 20, color: Colors.black),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () async {
                FocusScope.of(context).requestFocus(FocusNode());
                if (homeController.validateNameAndPassword()) {
                  var status = await homeController.getInformationView();
                  status == true
                      ? showDoneDialog(context)
                      : showErrorDialog(context);
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text("Tên và số điện thoại không được để trống"),
                  ));
                }
              },
              child: const Text("Check in"),
            ),
          ],
        ),
      ),
    );
  }

  showDoneDialog(BuildContext context) => showDialog(
      context: context,
      builder: (context) => Dialog(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Lottie.asset('assets/success.json'),
                const Text(
                  'Check in Success',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                )
              ],
            ),
          ));

  showErrorDialog(BuildContext context) => showDialog(
      context: context,
      builder: (context) => Dialog(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Lottie.asset('assets/error.json'),
                const Text(
                  'Check in Error',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                )
              ],
            ),
          ));
}

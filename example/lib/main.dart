

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'screen/scanner_screen.dart';

void main() => runApp(MaterialApp(
  theme: ThemeData(
    splashColor: Colors.transparent,
    highlightColor: Colors.transparent,
  ),
  home: Scanner(),)
    // ScannerState()
    // const MaterialApp(home: ScannerState())
);

class ScannerState extends StatelessWidget {
  const ScannerState({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: SplashScreenState(),
    );
  }

}

class SplashScreenState extends StatefulWidget {
  const SplashScreenState({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SplashScreen();
  }
}

class SplashScreen extends State<SplashScreenState>{

  @override
  void initState() {
    _navigateToHome();
    super.initState();
  }

  _navigateToHome() async {
    await Future.delayed(const Duration(milliseconds: 2500), () {});
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Scanner()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
      Center(child: SvgPicture.asset('assets/tepa_qr.svg'),),);
  }
}



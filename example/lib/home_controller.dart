
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:qr_code_scanner_example/checkin_service/checkin_service.dart';

class HomeController extends GetxController {

  late TextEditingController username;
  late TextEditingController phoneMobile;
  late CheckInService _checkInService;
  var statusCheckIn = false.obs;
  late PageController pageViewController;
  var selectedIndex = 0.obs;

  @override
  void onInit() {
    pageViewController = PageController();
    username = TextEditingController();
    phoneMobile = TextEditingController();
    _checkInService = CheckInService(Dio());
    super.onInit();
  }

  @override
  void onClose() {
    pageViewController.dispose();
    username.dispose();
    phoneMobile.dispose();
    super.onClose();
  }

  getInformationView() async {
    final status = await _checkInService.setInformationCheckIn("${username.text} ${phoneMobile.text}");
    return status;
  }


  void onItemTapped(int index) {
    selectedIndex.value = index;
    pageViewController.animateToPage(index, duration: const Duration(milliseconds: 200), curve: Curves.bounceOut);
  }

  bool validateNameAndPassword() {
    return username.text.isNotEmpty && phoneMobile.text.isNotEmpty;
  }


}
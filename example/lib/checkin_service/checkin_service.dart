
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';


class CheckInService {
  CheckInService(this._dio);
  final Dio _dio;

  Future<bool> setInformationCheckIn(String information) async {
    try {
      final response = await _dio.post('https://erp-api.cenpush.com/crm-api/index.php/app/webhook/checkin', data: {
        "code": information
      });
      if(response.statusCode == 200) {
        return Future.value(true);
      }
      return Future.value(false);
    } on DioError catch (dioError) {
      if (kDebugMode) {
        print(dioError);
      }
      return Future.value(false);
    }
  }
}